Tue May 17 17:49:46 2016 Test Report File creation: "TR_17May2016_174946.txt"
***************************************************************************************************
*                                    UNIVERSAL CAN GATEWAY TESTER                                 *
*                                      V2.1  - 10 February 2016                                   *
***************************************************************************************************
===================================================================================================
=                                           CYCLING START                                         =
=                                                                                                 =
=                                      Tue May 17 17:50:00 2016                                   =
=                                      start time:00:00:00.000                                    =
=                                                                                                 =
===================================================================================================
00:00:14.467 Cycling:1/1
###################################################################################################
#                                          TEST SUITE START                                       #
#                                                                                                 #
#                                      Tue May 17 17:50:00 2016                                   #
#                                      start time:00:00:14.467                                    #
#                   Test Configuration file: TestConfiguration.txt   - status: OK                 #
#                                                                                                 #
###################################################################################################
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:14.468                                    *
*                                                                                                 *
***************************************************************************************************
00:00:14.469 TEST CASE: " Get Bootloader Version From Bootloader"
00:00:14.470 VERSION: "V1 - 29 May 2015"
00:00:14.471 Reseting all the variables.
00:00:14.472 ---------------------------------------------------------------------------
00:00:14.473 Reseting all the variables.
00:00:14.474 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:14.475 Waiting for 1000ms 
00:00:15.475 "GE_R915GE_BL109_FWV_F_001"
00:00:15.476 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 18 10 03 0a 00 01 00 
00:00:15.477 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 18 10 03 00 00 00 00 
00:00:15.478 Checking:"" all registered messages for 500ms
00:00:15.480 Checking expected message(s) (remaining time:498ms)...............................ok
00:00:15.481 Waiting for 1000ms 
00:00:16.481 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:02.014                             *
*                                  CAN Test script file status: OK                                *
*              \UCGT_V2.0\TestCollimatorRalco\General\Get Bootloader VersionFromBL.txt            *
*                             " Get Bootloader Version From Bootloader"                           *
*                                         "V1 - 29 May 2015"                                      *
*                                                                                                 *
*                               Passed: 1   - Failed: 0    - Errors: 0                            *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
00:00:16.485 Stopping the cycling
===================================================================================================
=                                      Tue May 17 17:50:03 2016                                   =
=                                 Cycling Execution time:00:00:02.018                             =
=                                                                                                 =
=                               Passed: 1   - Failed: 0    - Errors: 0                            =
=                                      CYCLING RESULT : PASSED.                                   =
=                                                                                                 =
===================================================================================================
Closing test report file.
