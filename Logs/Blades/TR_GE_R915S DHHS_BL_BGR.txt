Fri Jun 10 17:33:43 2016 Test Report File creation: "TR_10Jun2016_173343.txt"
***************************************************************************************************
*                                    UNIVERSAL CAN GATEWAY TESTER                                 *
*                                      V2.1  - 10 February 2016                                   *
***************************************************************************************************
===================================================================================================
=                                           CYCLING START                                         =
=                                                                                                 =
=                                      Fri Jun 10 17:33:46 2016                                   =
=                                      start time:00:00:00.000                                    =
=                                                                                                 =
===================================================================================================
00:00:02.744 Cycling:1/1
###################################################################################################
#                                          TEST SUITE START                                       #
#                                                                                                 #
#                                      Fri Jun 10 17:33:46 2016                                   #
#                                      start time:00:00:02.744                                    #
#                   Test Configuration file: TestConfiguration.txt   - status: OK                 #
#                                                                                                 #
###################################################################################################
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:02.745                                    *
*                                                                                                 *
***************************************************************************************************
00:00:02.746 TEST CASE: " GE_R915S DHHS_BL_BGR_P_001"
00:00:02.747 VERSION: "V1 - 29 May 2015"
00:00:02.748 Reseting all the variables.
00:00:02.749 "Reseting and starting the device..."
00:00:02.750 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:02.751 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:13.130 Checking expected message(s) (remaining time:9621ms)..............................ok
00:00:13.131 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:13.630 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:13.631 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:13.632 Sending  :"START" CAN1 0x000 dlc:2 data:01 07 
00:00:13.633 Expecting:"OP" CAN1 0x707 dlc:1 data:05  for 20000ms
00:00:15.632 Checking expected message(s) (remaining time:18001ms).............................ok
00:00:15.633 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:15.634 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 00 60 00 00 00 00 00 
00:00:15.635 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2b 00 60 00 c8 19 00 00 
00:00:15.636 Checking:"" all registered messages for 1000ms
00:00:15.638 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:15.639 ---------------------------------------------------------------------------
00:00:15.640 Reseting all the variables.
00:00:15.641 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:15.642 Waiting for 1000ms 
00:00:16.642 Listening:"TxPDO" CAN1 0x187 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:16.643 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:16.644 Checking:"" all registered messages for 1000ms
00:00:16.851 Checking expected message(s) (remaining time:792ms)...............................ok
00:00:16.852 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:16.853 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:16.854 Checking:"" all registered messages for 1000ms
00:00:17.069 Checking expected message(s) (remaining time:785ms)...............................ok
00:00:17.070 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:17.071 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:17.072 Checking:"" all registered messages for 1000ms
00:00:17.651 Checking expected message(s) (remaining time:420ms)...............................ok
00:00:17.652 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:17.653 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:17.654 Checking:"" all registered messages for 1000ms
00:00:18.235 Checking expected message(s) (remaining time:418ms)...............................ok
00:00:18.236 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:18.237 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:18.238 Checking:"" all registered messages for 1000ms
00:00:18.867 Checking expected message(s) (remaining time:371ms)...............................ok
00:00:18.868 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:18.869 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:18.870 Checking:"" all registered messages for 1000ms
00:00:19.500 Checking expected message(s) (remaining time:369ms)...............................ok
00:00:19.501 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:19.502 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:19.503 Checking:"" all registered messages for 1000ms
00:00:20.081 Checking expected message(s) (remaining time:421ms)...............................ok
00:00:20.082 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:20.083 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:20.084 Checking:"" all registered messages for 1000ms
00:00:20.663 Checking expected message(s) (remaining time:420ms)...............................ok
00:00:20.664 Waiting for 1000ms 
00:00:21.664 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:18.921                             *
*                                  CAN Test script file status: OK                                *
*           \UCGT_V2.0\TestCollimator_R915S_DHHS\Blades\Blades General Requirements_1.txt         *
*                                   " GE_R915S DHHS_BL_BGR_P_001"                                 *
*                                         "V1 - 29 May 2015"                                      *
*                                                                                                 *
*                              Passed: 14   - Failed: 0    - Errors: 0                            *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:21.668                                    *
*                                                                                                 *
***************************************************************************************************
00:00:21.669 TEST CASE: " GE_R915S DHHS_BL_BGR_D_001"
00:00:21.670 VERSION: "V1 - 29 May 2015"
00:00:21.671 Reseting all the variables.
00:00:21.672 "Reseting and starting the device..."
00:00:21.673 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:21.674 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:31.107 Checking expected message(s) (remaining time:10567ms).............................ok
00:00:31.108 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:31.607 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:31.608 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:31.609 Sending  :"START" CAN1 0x000 dlc:2 data:01 07 
00:00:31.610 Expecting:"OP" CAN1 0x707 dlc:1 data:05  for 20000ms
00:00:33.608 Checking expected message(s) (remaining time:18001ms).............................ok
00:00:33.609 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:33.610 ---------------------------------------------------------------------------
00:00:33.611 Reseting all the variables.
00:00:33.612 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:33.613 Waiting for 1000ms 
00:00:34.613 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.614 Sending  :"RxPDO2" CAN1 0x207 dlc:1 data:64 
00:00:34.615 Checking:"" all registered messages for 1000ms
00:00:34.618 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 01 00 00 
00:00:34.618 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:34.619 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.620 Sending  :"RxPDO2" CAN1 0x207 dlc:2 data:64 00 
00:00:34.621 Checking:"" all registered messages for 1000ms
00:00:34.623 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 02 00 00 
00:00:34.623 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:34.624 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.625 Sending  :"RxPDO2" CAN1 0x207 dlc:3 data:64 00 e8 
00:00:34.626 Checking:"" all registered messages for 1000ms
00:00:34.628 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 03 00 00 
00:00:34.628 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:34.629 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.630 Sending  :"RxPDO2" CAN1 0x207 dlc:4 data:64 00 e8 03 
00:00:34.631 Checking:"" all registered messages for 1000ms
00:00:34.633 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 04 00 00 
00:00:34.633 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:34.634 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.635 Sending  :"RxPDO2" CAN1 0x207 dlc:5 data:64 00 e8 03 d0 
00:00:34.636 Checking:"" all registered messages for 1000ms
00:00:34.638 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 05 00 00 
00:00:34.638 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:34.639 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.640 Sending  :"RxPDO2" CAN1 0x207 dlc:6 data:64 00 e8 03 d0 0f 
00:00:34.641 Checking:"" all registered messages for 1000ms
00:00:34.643 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 06 00 00 
00:00:34.643 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:34.644 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.645 Sending  :"RxPDO2" CAN1 0x207 dlc:7 data:64 00 e8 03 d0 0f e8 
00:00:34.646 Checking:"" all registered messages for 1000ms
00:00:34.648 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 07 00 00 
00:00:34.648 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:34.649 Listening:"EMCY" CAN1 0x087 dlc:8 data:10 82 ** ** ** ** ** ** 
00:00:34.650 Sending  :"RxPDO2" CAN1 0x207 dlc:9 data:64 00 e8 03 d0 0f e8 03 00 
00:00:34.651 Checking:"" all registered messages for 1000ms
00:00:34.653 Reporting message:0x087 on CAN1 dlc=8 data:10 82 00 04 08 09 00 00 
00:00:34.653 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:34.654 Waiting for 1000ms 
00:00:35.654 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:13.987                             *
*                                  CAN Test script file status: OK                                *
*           \UCGT_V2.0\TestCollimator_R915S_DHHS\Blades\Blades General Requirements_2.txt         *
*                                   " GE_R915S DHHS_BL_BGR_D_001"                                 *
*                                         "V1 - 29 May 2015"                                      *
*                                                                                                 *
*                              Passed: 13   - Failed: 0    - Errors: 0                            *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:35.658                                    *
*                                                                                                 *
***************************************************************************************************
00:00:35.659 TEST CASE: " GE_R915S DHHS_BL_BGR_F_001"
00:00:35.660 VERSION: "V1 - 29 May 2015"
00:00:35.661 Reseting all the variables.
00:00:35.662 "Reseting and starting the device..."
00:00:35.663 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:35.664 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:45.149 Checking expected message(s) (remaining time:10515ms).............................ok
00:00:45.150 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:45.649 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:45.650 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:45.651 Sending  :"START" CAN1 0x000 dlc:2 data:01 07 
00:00:45.652 Expecting:"OP" CAN1 0x707 dlc:1 data:05  for 20000ms
00:00:47.651 Checking expected message(s) (remaining time:18001ms).............................ok
00:00:47.652 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:47.653 ---------------------------------------------------------------------------
00:00:47.654 Reseting all the variables.
00:00:47.655 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:47.656 Waiting for 1000ms 
00:00:48.656 Listening:"TxPDO" CAN1 0x187 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:48.657 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:48.658 Waiting for 30ms ""
00:00:48.688 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:48.689 Waiting for 30ms ""
00:00:48.719 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:48.720 Waiting for 30ms ""
00:00:48.750 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:48.751 Waiting for 30ms ""
00:00:48.781 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:48.782 Waiting for 30ms ""
00:00:48.812 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:48.813 Waiting for 30ms ""
00:00:48.843 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:48.844 Waiting for 30ms ""
00:00:48.874 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:48.875 Waiting for 30ms ""
00:00:48.905 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:7e ff 8e 06 3c 0a 8e 06 
00:00:48.906 Checking:"" all registered messages for 1000ms
00:00:49.076 Checking expected message(s) (remaining time:829ms)...............................ok
00:00:49.077 Waiting for 3000ms ""
00:00:52.077 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:52.078 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:52.079 Waiting for 30ms ""
00:00:52.109 Sending  :"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:52.110 Waiting for 30ms ""
00:00:52.140 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:52.141 Waiting for 30ms ""
00:00:52.171 Sending  :"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:52.172 Waiting for 30ms ""
00:00:52.202 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:52.203 Waiting for 30ms ""
00:00:52.233 Sending  :"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:52.234 Waiting for 30ms ""
00:00:52.264 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:52.265 Waiting for 30ms ""
00:00:52.295 Sending  :"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:52.296 Waiting for 30ms ""
00:00:52.326 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 a2 fe 3c 0a 8e 06 
00:00:52.327 Checking:"" all registered messages for 1000ms
00:00:52.657 Checking expected message(s) (remaining time:669ms)...............................ok
00:00:52.658 Waiting for 3000ms ""
00:00:55.658 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:55.659 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:55.660 Waiting for 30ms ""
00:00:55.690 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:55.691 Waiting for 30ms ""
00:00:55.721 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:55.722 Waiting for 30ms ""
00:00:55.752 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:55.753 Waiting for 30ms ""
00:00:55.783 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:55.784 Waiting for 30ms ""
00:00:55.814 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:55.815 Waiting for 30ms ""
00:00:55.845 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:55.846 Waiting for 30ms ""
00:00:55.876 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:55.877 Waiting for 30ms ""
00:00:55.907 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 26 02 8e 06 
00:00:55.908 Checking:"" all registered messages for 1000ms
00:00:56.511 Checking expected message(s) (remaining time:397ms)...............................ok
00:00:56.512 Waiting for 3000ms ""
00:00:59.512 Listening:"TxPDO" CAN1 0x187 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:59.513 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:59.514 Waiting for 30ms ""
00:00:59.544 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:59.545 Waiting for 30ms ""
00:00:59.575 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:59.576 Waiting for 30ms ""
00:00:59.606 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:59.607 Waiting for 30ms ""
00:00:59.637 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:59.638 Waiting for 30ms ""
00:00:59.668 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:59.669 Waiting for 30ms ""
00:00:59.699 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:59.700 Waiting for 30ms ""
00:00:59.730 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a 8e 06 
00:00:59.731 Waiting for 30ms ""
00:00:59.761 Sending  :"RxPDO2" CAN1 0x207 dlc:8 data:82 00 8e 06 3c 0a a2 fe 
00:00:59.762 Checking:"" all registered messages for 1000ms
00:01:00.317 Checking expected message(s) (remaining time:444ms)...............................ok
00:01:00.318 Waiting for 1000ms 
00:01:01.318 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:25.661                             *
*                                  CAN Test script file status: OK                                *
*           \UCGT_V2.0\TestCollimator_R915S_DHHS\Blades\Blades General Requirements_3.txt         *
*                                   " GE_R915S DHHS_BL_BGR_F_001"                                 *
*                                         "V1 - 29 May 2015"                                      *
*                                                                                                 *
*                               Passed: 9   - Failed: 0    - Errors: 0                            *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
###################################################################################################
#                                      Fri Jun 10 17:34:45 2016                                   #
#                               Test Suite Execution time:00:00:58.576                            #
#                                                                                                 #
#                              Passed: 36   - Failed: 0    - Errors: 0                            #
#                                     TEST SUITE RESULT : PASSED.                                 #
#                                                                                                 #
###################################################################################################
00:01:01.322 Stopping the cycling
===================================================================================================
=                                      Fri Jun 10 17:34:45 2016                                   =
=                                 Cycling Execution time:00:00:58.578                             =
=                                                                                                 =
=                              Passed: 36   - Failed: 0    - Errors: 0                            =
=                                      CYCLING RESULT : PASSED.                                   =
=                                                                                                 =
===================================================================================================
Closing test report file.
