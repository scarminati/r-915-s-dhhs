Tue Jun 21 13:59:11 2016 Test Report File creation: "TR_21Jun2016_135911.txt"
***************************************************************************************************
*                                    UNIVERSAL CAN GATEWAY TESTER                                 *
*                                      V2.1  - 10 February 2016                                   *
***************************************************************************************************
===================================================================================================
=                                           CYCLING START                                         =
=                                                                                                 =
=                                      Tue Jun 21 13:59:12 2016                                   =
=                                      start time:00:00:00.000                                    =
=                                                                                                 =
===================================================================================================
00:00:01.676 Cycling:1/1
###################################################################################################
#                                          TEST SUITE START                                       #
#                                                                                                 #
#                                      Tue Jun 21 13:59:12 2016                                   #
#                                      start time:00:00:01.676                                    #
#                   Test Configuration file: TestConfiguration.txt   - status: OK                 #
#                                                                                                 #
###################################################################################################
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:01.677                                    *
*                                                                                                 *
***************************************************************************************************
00:00:01.678 TEST CASE: " GE_R915S DHHS_FS_FMEA_D_001"
00:00:01.679 VERSION: "V1 - 16 June 2016"
00:00:01.680 Reseting all the variables.
00:00:01.681 "Reseting and starting the device in Pre Operational state..."
00:00:01.682 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:01.683 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:10.877 Checking expected message(s) (remaining time:10806ms).............................ok
00:00:10.878 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:11.377 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:11.378 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:11.379 ---------------------------------------------------------------------------
00:00:11.380 Reseting all the variables.
00:00:11.381 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:11.382 Waiting for 1000ms 
00:00:12.382 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:12.383 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 00 ** ** ** ** 
00:00:12.384 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 00 ff 00 00 00 
00:00:12.385 Checking:"" all registered messages for 1000ms
00:00:12.388 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 31 00 09 06 
00:00:12.388 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:12.389 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:12.390 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 48 ** ** ** ** 
00:00:12.391 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 48 01 00 00 00 
00:00:12.392 Checking:"" all registered messages for 1000ms
00:00:12.396 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:12.396 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:12.397 Waiting for 500ms ""
00:00:12.897 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:12.898 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 01 30 f5 ** ** ** ** 
00:00:12.899 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 01 30 f5 00 00 00 00 
00:00:12.900 Checking:"" all registered messages for 1000ms
00:00:12.903 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:12.903 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:12.904 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:12.905 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 00 ** ** ** ** 
00:00:12.906 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 00 c8 00 00 00 
00:00:12.907 Checking:"" all registered messages for 1000ms
00:00:12.910 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 31 00 09 06 
00:00:12.910 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:12.911 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:12.912 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 72 ** ** ** ** 
00:00:12.913 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 72 00 00 00 00 
00:00:12.914 Checking:"" all registered messages for 1000ms
00:00:12.917 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:12.917 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:12.918 Waiting for 500ms ""
00:00:13.418 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:13.419 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 01 30 49 ** ** ** ** 
00:00:13.420 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 01 30 49 00 00 00 00 
00:00:13.421 Checking:"" all registered messages for 1000ms
00:00:13.424 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:13.424 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:13.425 Waiting for 1000ms ""
00:00:14.425 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.426 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 84 ** ** ** ** 
00:00:14.427 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 84 02 00 00 00 
00:00:14.428 Checking:"" all registered messages for 1000ms
00:00:14.431 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.431 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:14.432 Waiting for 500ms ""
00:00:14.932 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.933 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 01 30 7a ** ** ** ** 
00:00:14.934 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 01 30 7a 00 00 00 00 
00:00:14.935 Checking:"" all registered messages for 1000ms
00:00:14.938 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.938 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:14.939 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.940 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 8b ** ** ** ** 
00:00:14.941 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 8b 03 00 00 00 
00:00:14.942 Checking:"" all registered messages for 1000ms
00:00:14.946 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.946 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:14.947 Waiting for 500ms ""
00:00:15.447 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.448 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 01 30 92 ** ** ** ** 
00:00:15.449 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 01 30 92 00 00 00 00 
00:00:15.450 Checking:"" all registered messages for 1000ms
00:00:15.454 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.454 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:15.455 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.456 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 10 ** ** ** ** 
00:00:15.457 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 10 04 00 00 00 
00:00:15.458 Checking:"" all registered messages for 1000ms
00:00:15.462 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.462 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:15.463 Waiting for 500ms ""
00:00:15.963 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.964 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 01 30 30 ** ** ** ** 
00:00:15.965 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 01 30 30 00 00 00 00 
00:00:15.966 Checking:"" all registered messages for 1000ms
00:00:15.970 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.970 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:15.971 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.972 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 00 ** ** ** ** 
00:00:15.973 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 00 05 00 00 00 
00:00:15.974 Checking:"" all registered messages for 1000ms
00:00:15.977 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 31 00 09 06 
00:00:15.977 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:15.978 Waiting for 500ms ""
00:00:16.478 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:16.479 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 30 00 ** ** ** ** 
00:00:16.480 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 30 00 fe 00 00 00 
00:00:16.481 Checking:"" all registered messages for 1000ms
00:00:16.484 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 31 00 09 06 
00:00:16.484 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:16.485 Waiting for 1000ms 
00:00:17.485 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:15.809                             *
*                                  CAN Test script file status: OK                                *
*              \UCGT_V2.0\TestCollimator_R915S_DHHS\DFMEA\FS,InvalidCommandReceived.txt           *
*                                   " GE_R915S DHHS_FS_FMEA_D_001"                                *
*                                        "V1 - 16 June 2016"                                      *
*                                                                                                 *
*                                Passed: 17   - Failed: 0    - Errors                             *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
00:00:17.489 Stopping the cycling
===================================================================================================
=                                      Tue Jun 21 13:59:28 2016                                   =
=                                 Cycling Execution time:00:00:15.813                             =
=                                                                                                 =
=                                Passed: 17   - Failed: 0    - Errors                             =
=                                      CYCLING RESULT : PASSED.                                   =
=                                                                                                 =
===================================================================================================
Closing test report file.
