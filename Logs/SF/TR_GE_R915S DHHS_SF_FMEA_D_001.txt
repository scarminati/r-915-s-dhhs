Wed Nov 13 10:45:03 2019 Test Report File creation: "/Test Reports/TR_13Nov2019_104503.txt"
***************************************************************************************************
*                                    UNIVERSAL CAN GATEWAY TESTER                                 *
*                                      V2.1  - 18 January 2017                                    *
***************************************************************************************************
===================================================================================================
=                                           CYCLING START                                         =
=                                                                                                 =
=                                      Wed Nov 13 10:45:07 2019                                   =
=                                      start time:00:00:00.000                                    =
=                                                                                                 =
===================================================================================================
00:00:03.852 Cycling:1/1
###################################################################################################
#                                          TEST SUITE START                                       #
#                                                                                                 #
#                                      Wed Nov 13 10:45:07 2019                                   #
#                                      start time:00:00:03.852                                    #
#                   Test Configuration file: TestConfiguration.txt   - status: OK                 #
#                                                                                                 #
###################################################################################################
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:03.853                                    *
*                                                                                                 *
***************************************************************************************************
00:00:03.854 TEST CASE: " GE_R915S DHHS_SF_FMEA_D_001"
00:00:03.855 VERSION: "V1 - 16 June 2016"
00:00:03.856 Reseting all the variables.
00:00:03.857 "Resetting and starting the device in pre-Operational state..."
00:00:03.858 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:03.859 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:13.059 Checking expected message(s) (remaining time:10800ms).............................ok
00:00:13.060 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:13.559 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:13.560 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:13.561 ---------------------------------------------------------------------------
00:00:13.562 Reseting all the variables.
00:00:13.563 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:13.564 Waiting for 1000ms 
00:00:14.564 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.565 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 de ** ** ** ** 
00:00:14.566 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 de 00 00 00 00 
00:00:14.567 Checking:"" all registered messages for 1000ms
00:00:14.571 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.571 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:14.572 Waiting for 200ms ""
00:00:14.772 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.773 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 00 61 45 ** ** ** ** 
00:00:14.774 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 61 45 00 00 00 00 
00:00:14.775 Checking:"" all registered messages for 1000ms
00:00:14.778 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.778 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:14.779 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.780 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 f1 ** ** ** ** 
00:00:14.781 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 f1 01 00 00 00 
00:00:14.782 Checking:"" all registered messages for 1000ms
00:00:14.786 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.786 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:14.787 Waiting for 200ms ""
00:00:14.987 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.988 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 00 61 96 ** ** ** ** 
00:00:14.989 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 61 96 00 00 00 00 
00:00:14.990 Checking:"" all registered messages for 1000ms
00:00:14.994 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:14.994 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:14.995 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.996 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:14.997 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 42 ** ** ** ** 
00:00:14.998 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 42 02 00 00 00 
00:00:14.999 Checking:"" all registered messages for 1000ms
00:00:15.003 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.003 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:15.004 Waiting for 200ms ""
00:00:15.204 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.205 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 00 61 69 ** ** ** ** 
00:00:15.206 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 61 69 00 00 00 00 
00:00:15.207 Checking:"" all registered messages for 1000ms
00:00:15.211 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.211 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:15.212 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.213 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 00 ** ** ** ** 
00:00:15.214 Listening:"EMCY" CAN1 0x087 dlc:8 data:20 a0 ** ** 10 ** ** ** 
00:00:15.215 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 00 fe 00 00 00 
00:00:15.216 Checking:"" all registered messages for 1000ms
00:00:15.220 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:15.221 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 10 00 20 a0 
00:00:15.221 Checking expected message(s) (remaining time:994ms)...............................ok
00:00:15.222 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.223 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 55 ** ** ** ** 
00:00:15.224 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 55 04 00 00 00 
00:00:15.225 Checking:"" all registered messages for 1000ms
00:00:15.229 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.229 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:15.230 Waiting for 200ms ""
00:00:15.430 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.431 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 00 61 84 ** ** ** ** 
00:00:15.432 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 61 84 00 00 00 00 
00:00:15.433 Checking:"" all registered messages for 1000ms
00:00:15.436 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.436 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:15.437 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.438 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 09 ** ** ** ** 
00:00:15.439 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 09 03 00 00 00 
00:00:15.440 Checking:"" all registered messages for 1000ms
00:00:15.443 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.443 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:15.444 Waiting for 200ms ""
00:00:15.644 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.645 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:80 00 61 67 ** ** ** ** 
00:00:15.646 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 61 67 00 00 00 00 
00:00:15.647 Checking:"" all registered messages for 1000ms
00:00:15.651 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 11 00 09 06 
00:00:15.651 Checking expected message(s) (remaining time:996ms)...............................ok
00:00:15.652 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.653 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 00 ** ** ** ** 
00:00:15.654 Listening:"EMCY" CAN1 0x087 dlc:8 data:20 a0 ** ** 10 ** ** ** 
00:00:15.655 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 00 05 00 00 00 
00:00:15.656 Checking:"" all registered messages for 1000ms
00:00:15.659 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:15.660 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 10 00 20 a0 
00:00:15.660 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:15.661 Listening:"EMCY" CAN1 0x087 dlc:8 data:00 82 00 27 ** ** ** ** 
00:00:15.662 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 00 61 00 ** ** ** ** 
00:00:15.663 Listening:"EMCY" CAN1 0x087 dlc:8 data:20 a0 ** ** 10 ** ** ** 
00:00:15.664 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:2f 00 61 00 ff 00 00 00 
00:00:15.665 Checking:"" all registered messages for 1000ms
00:00:15.668 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:15.669 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 10 00 20 a0 
00:00:15.669 Checking expected message(s) (remaining time:995ms)...............................ok
00:00:15.670 Waiting for 1000ms 
00:00:16.670 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:12.818                             *
*                                  CAN Test script file status: OK                                *
*            \UCGT_V2.0\TP\TestCollimator_R915S_DHHS\DFMEA\SF,InvalidCommandReceived.txt          *
*                                   " GE_R915S DHHS_SF_FMEA_D_001"                                *
*                                        "V1 - 16 June 2016"                                      *
*                                                                                                 *
*                                Passed: 16   - Failed: 0    - Errors                             *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
00:00:16.674 Stopping the cycling
===================================================================================================
=                                      Wed Nov 13 10:45:20 2019                                   =
=                                 Cycling Execution time:00:00:12.822                             =
=                                                                                                 =
=                                Passed: 16   - Failed: 0    - Errors                             =
=                                      CYCLING RESULT : PASSED.                                   =
=                                                                                                 =
===================================================================================================
Closing test report file.
