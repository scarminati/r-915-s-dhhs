Wed Nov 13 10:11:47 2019 Test Report File creation: "/Test Reports/TR_13Nov2019_101147.txt"
***************************************************************************************************
*                                    UNIVERSAL CAN GATEWAY TESTER                                 *
*                                      V2.1  - 18 January 2017                                    *
***************************************************************************************************
===================================================================================================
=                                           CYCLING START                                         =
=                                                                                                 =
=                                      Wed Nov 13 10:11:50 2019                                   =
=                                      start time:00:00:00.000                                    =
=                                                                                                 =
===================================================================================================
00:00:03.254 Cycling:1/1
###################################################################################################
#                                          TEST SUITE START                                       #
#                                                                                                 #
#                                      Wed Nov 13 10:11:50 2019                                   #
#                                      start time:00:00:03.254                                    #
#                   Test Configuration file: TestConfiguration.txt   - status: OK                 #
#                                                                                                 #
###################################################################################################
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:03.255                                    *
*                                                                                                 *
***************************************************************************************************
00:00:03.256 TEST CASE: " GE_R915S DHHS_SF_GFMC_F_001"
00:00:03.257 VERSION: "V1 - 29 May 2015"
00:00:03.258 Reseting all the variables.
00:00:03.259 "Reseting and starting the device..."
00:00:03.260 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:03.261 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:13.961 Checking expected message(s) (remaining time:9299ms)..............................ok
00:00:13.962 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:14.461 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:14.462 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:14.463 Sending  :"START" CAN1 0x000 dlc:2 data:01 07 
00:00:14.464 Expecting:"OP" CAN1 0x707 dlc:1 data:05  for 20000ms
00:00:16.463 Checking expected message(s) (remaining time:18001ms).............................ok
00:00:16.464 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:16.465 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 01 00 00 00 
00:00:16.466 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:16.467 Checking:"" all registered messages for 1000ms
00:00:16.469 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:16.470 ---------------------------------------------------------------------------
00:00:16.471 Reseting all the variables.
00:00:16.472 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:16.473 Waiting for 1000ms 
00:00:17.473 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:01 
00:00:17.474 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:00 
00:00:17.475 Checking:"" all registered messages for 1000ms
00:00:17.476 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:17.477 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 01 00 00 00 
00:00:17.477 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:17.478 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:17.479 Checking:"" all registered messages for 1000ms
00:00:17.481 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.482 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:01 
00:00:17.483 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:01 
00:00:17.484 Checking:"" all registered messages for 1000ms
00:00:17.487 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.488 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 02 00 00 00 
00:00:17.489 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:17.490 Checking:"" all registered messages for 1000ms
00:00:17.492 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.493 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:01 
00:00:17.494 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:ff 
00:00:17.495 Checking:"" all registered messages for 1000ms
00:00:17.497 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:17.498 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 02 00 00 00 
00:00:17.498 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:17.499 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:17.500 Checking:"" all registered messages for 1000ms
00:00:17.502 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.503 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:03 
00:00:17.504 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:03 
00:00:17.505 Checking:"" all registered messages for 1000ms
00:00:17.739 Checking expected message(s) (remaining time:766ms)...............................ok
00:00:17.740 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 03 00 00 00 
00:00:17.741 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:17.742 Checking:"" all registered messages for 1000ms
00:00:17.744 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.745 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:03 
00:00:17.746 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:c4 
00:00:17.747 Checking:"" all registered messages for 1000ms
00:00:17.749 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:17.750 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 03 00 00 00 
00:00:17.750 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:17.751 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:17.752 Checking:"" all registered messages for 1000ms
00:00:17.754 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.755 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:04 
00:00:17.756 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:04 
00:00:17.757 Checking:"" all registered messages for 1000ms
00:00:17.921 Checking expected message(s) (remaining time:836ms)...............................ok
00:00:17.922 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 04 00 00 00 
00:00:17.923 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:17.924 Checking:"" all registered messages for 1000ms
00:00:17.927 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:17.928 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:02 
00:00:17.929 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:02 
00:00:17.930 Checking:"" all registered messages for 1000ms
00:00:18.163 Checking expected message(s) (remaining time:766ms)...............................ok
00:00:18.164 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 05 00 00 00 
00:00:18.165 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:18.166 Checking:"" all registered messages for 1000ms
00:00:18.169 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:18.170 Listening:"TxPDO2" CAN1 0x287 dlc:1 data:02 
00:00:18.171 Sending  :"RxPDO2" CAN1 0x307 dlc:1 data:a3 
00:00:18.172 Checking:"" all registered messages for 1000ms
00:00:18.173 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:18.174 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 00 23 00 05 00 00 00 
00:00:18.175 Reporting message:0x087 on CAN1 dlc=8 data:20 a0 00 00 10 00 00 00 
00:00:18.175 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 00 23 00 00 00 00 00 
00:00:18.176 Checking:"" all registered messages for 1000ms
00:00:18.179 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:18.180 Waiting for 1000ms 
00:00:19.180 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:15.926                             *
*                                  CAN Test script file status: OK                                *
*             \UCGT_V2.0\TP\TestCollimator_R915S_DHHS\SF\Get Filter Movement Cycles.txt           *
*                                   " GE_R915S DHHS_SF_GFMC_F_001"                                *
*                                         "V1 - 29 May 2015"                                      *
*                                                                                                 *
*                                Passed: 22   - Failed: 0    - Errors                             *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
00:00:19.184 Stopping the cycling
===================================================================================================
=                                      Wed Nov 13 10:12:06 2019                                   =
=                                 Cycling Execution time:00:00:15.930                             =
=                                                                                                 =
=                                Passed: 22   - Failed: 0    - Errors                             =
=                                      CYCLING RESULT : PASSED.                                   =
=                                                                                                 =
===================================================================================================
Closing test report file.
