Wed Nov 13 11:32:57 2019 Test Report File creation: "/Test Reports/TR_13Nov2019_113257.txt"
***************************************************************************************************
*                                    UNIVERSAL CAN GATEWAY TESTER                                 *
*                                      V2.1  - 18 January 2017                                    *
***************************************************************************************************
===================================================================================================
=                                           CYCLING START                                         =
=                                                                                                 =
=                                      Wed Nov 13 11:33:00 2019                                   =
=                                      start time:00:00:00.000                                    =
=                                                                                                 =
===================================================================================================
00:00:02.724 Cycling:1/1
###################################################################################################
#                                          TEST SUITE START                                       #
#                                                                                                 #
#                                      Wed Nov 13 11:33:00 2019                                   #
#                                      start time:00:00:02.724                                    #
#                   Test Configuration file: TestConfiguration.txt   - status: OK                 #
#                                                                                                 #
###################################################################################################
***************************************************************************************************
*                                          TEST CASE START                                        *
*                                      start time:00:00:02.725                                    *
*                                                                                                 *
***************************************************************************************************
00:00:02.726 TEST CASE: " GE_R915S DHHS_XL_TTL_F_001"
00:00:02.727 VERSION: "V1 - 29 May 2015"
00:00:02.728 Reseting all the variables.
00:00:02.729 "Resetting and starting the device in pre-Operational state..."
00:00:02.730 Sending  :"RESET" CAN1 0x000 dlc:2 data:81 07 
00:00:02.731 Expecting:"BOOTUP" CAN1 0x707 dlc:1 data:00  for 20000ms
00:00:12.135 Checking expected message(s) (remaining time:10596ms).............................ok
00:00:12.136 Expecting:"PreOp" CAN1 0x707 dlc:1 data:7f  for 5000ms
00:00:12.635 Checking expected message(s) (remaining time:4500ms)..............................ok
00:00:12.636 Verify count:"No EMCY" on CAN1 0x87 Expected:0 Observed:0.........................ok
00:00:12.637 ---------------------------------------------------------------------------
00:00:12.638 Reseting all the variables.
00:00:12.639 Registering :"Emergencies errors" CAN1 0x087 to be reported.
00:00:12.640 Waiting for 1000ms 
00:00:13.640 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 02 63 00 ** ** ** ** 
00:00:13.641 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 00 00 00 00 
00:00:13.642 Checking:"" all registered messages for 1000ms
00:00:13.644 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.645 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 d0 07 00 00 
00:00:13.645 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 32 00 09 06 
00:00:13.646 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.647 Checking:"" all registered messages for 1000ms
00:00:13.649 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.650 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 02 63 00 ** ** ** ** 
00:00:13.651 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 01 00 00 00 
00:00:13.652 Checking:"" all registered messages for 1000ms
00:00:13.654 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.655 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 d0 07 00 00 
00:00:13.655 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 32 00 09 06 
00:00:13.656 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.657 Checking:"" all registered messages for 1000ms
00:00:13.659 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.660 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:80 02 63 00 ** ** ** ** 
00:00:13.661 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 f3 01 00 00 
00:00:13.662 Checking:"" all registered messages for 1000ms
00:00:13.664 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.665 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 d0 07 00 00 
00:00:13.665 Reporting message:0x087 on CAN1 dlc=8 data:00 82 00 27 32 00 09 06 
00:00:13.666 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.667 Checking:"" all registered messages for 1000ms
00:00:13.669 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.670 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 02 63 00 ** ** ** ** 
00:00:13.671 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 f4 01 00 00 
00:00:13.672 Checking:"" all registered messages for 1000ms
00:00:13.674 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.675 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 f4 01 00 00 
00:00:13.676 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.677 Checking:"" all registered messages for 1000ms
00:00:13.679 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.680 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 02 63 00 ** ** ** ** 
00:00:13.681 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 f5 01 00 00 
00:00:13.682 Checking:"" all registered messages for 1000ms
00:00:13.684 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.685 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 f5 01 00 00 
00:00:13.686 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.687 Checking:"" all registered messages for 1000ms
00:00:13.689 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.690 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 02 63 00 ** ** ** ** 
00:00:13.691 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 f9 02 00 00 
00:00:13.692 Checking:"" all registered messages for 1000ms
00:00:13.694 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.695 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 f9 02 00 00 
00:00:13.696 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.697 Checking:"" all registered messages for 1000ms
00:00:13.699 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.700 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 02 63 00 ** ** ** ** 
00:00:13.701 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 c8 06 00 00 
00:00:13.702 Checking:"" all registered messages for 1000ms
00:00:13.704 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.705 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 c8 06 00 00 
00:00:13.706 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.707 Checking:"" all registered messages for 1000ms
00:00:13.709 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.710 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 02 63 00 ** ** ** ** 
00:00:13.711 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 fe ff ff ff 
00:00:13.712 Checking:"" all registered messages for 1000ms
00:00:13.714 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.715 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 fe ff ff ff 
00:00:13.716 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.717 Checking:"" all registered messages for 1000ms
00:00:13.719 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.720 Listening:"SSDO_Write" CAN1 0x587 dlc:8 data:60 02 63 00 ** ** ** ** 
00:00:13.721 Sending  :"CSDO_Write" CAN1 0x607 dlc:8 data:23 02 63 00 ff ff ff ff 
00:00:13.722 Checking:"" all registered messages for 1000ms
00:00:13.724 Checking expected message(s) (remaining time:997ms)...............................ok
00:00:13.725 Listening:"SSDO_Read" CAN1 0x587 dlc:8 data:43 02 63 00 ff ff ff ff 
00:00:13.726 Sending  :"CSDO_Read" CAN1 0x607 dlc:8 data:40 02 63 00 00 00 00 00 
00:00:13.727 Checking:"" all registered messages for 1000ms
00:00:13.729 Checking expected message(s) (remaining time:998ms)...............................ok
00:00:13.730 Waiting for 1000ms 
00:00:14.730 Reseting all the variables.
***************************************************************************************************
*                               Test case Execution time:00:00:12.006                             *
*                                  CAN Test script file status: OK                                *
*               \UCGT_V2.0\TP\TestCollimator_R915S_DHHS\Light\TriggerTimeSimLight.txt             *
*                                   " GE_R915S DHHS_XL_TTL_F_001"                                 *
*                                         "V1 - 29 May 2015"                                      *
*                                                                                                 *
*                                Passed: 21   - Failed: 0    - Errors                             *
*                                     TEST CASE RESULT : PASSED.                                  *
*                                                                                                 *
***************************************************************************************************
00:00:14.734 Stopping the cycling
===================================================================================================
=                                      Wed Nov 13 11:33:12 2019                                   =
=                                 Cycling Execution time:00:00:12.010                             =
=                                                                                                 =
=                                Passed: 21   - Failed: 0    - Errors                             =
=                                      CYCLING RESULT : PASSED.                                   =
=                                                                                                 =
===================================================================================================
Closing test report file.
